{-# LANGUAGE RankNTypes       #-}
{-# LANGUAGE FlexibleContexts #-}
module PnP where

import System.Random (getStdRandom, randomR)


type Roll          = Int
type NumberOfRolls = Int
type Skill         = Int
type DieSize       = Int

rollDice :: Int -> Int -> IO [Int]
rollDice m n = sequenceA . replicate n . getStdRandom . randomR $ (1, m)

roll1Die :: Int -> IO Int
roll1Die = fmap head . (`rollDice` 1)

rollD20s :: Roll -> IO [Int]
rollD20s = rollDice 20

roll1D20 :: IO Int
roll1D20 = roll1Die 20

rollD100s :: Roll -> IO [Int]
rollD100s = rollDice 100

roll1D100 :: IO Int
roll1D100 = roll1Die 100

cthuluRollD100s :: Skill -> NumberOfRolls -> IO [String]
cthuluRollD100s = cthuluRolls 100

cthuluRoll1D100 :: Skill -> IO String
cthuluRoll1D100 skill = head <$> cthuluRollD100s skill 1

cthuluRoll1Die :: DieSize -> Skill -> IO String
cthuluRoll1Die ds s = head <$> cthuluRolls ds s 1

cthuluRolls :: DieSize -> Skill -> NumberOfRolls -> IO [String]
cthuluRolls sides skill numOfRolls = do
    results <- rollDice sides numOfRolls
    let successes = zip results (toSuccess skill <$> results)
    pure $ ppTuple <$> successes
  where
    ppTuple :: (Show a, Show b) => (a, b) -> String
    ppTuple (a, b) = show a <> " (" <> show b <> ")"

data Success
    = Fumble
    | Fail
    | Regular
    | Hard
    | Extreme
    | Critical
    deriving (Show)

toSuccess :: Skill -> Roll -> Success
toSuccess skill roll
    | roll == 1                  = Critical
    | roll <= skill `div` 5      = Extreme
    | roll <= skill `div` 2      = Hard
    | roll <= skill              = Regular
    | roll  > skill && roll < 95 = Fail
    | roll == 100                = Fumble
    | otherwise                  = if skill > 50 then Fail else Fumble
